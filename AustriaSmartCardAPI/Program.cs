﻿using System;
using PCSC;
using PCSC.Iso7816;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.Text;
using System.Diagnostics;
using Cashregister.Smartcard;

namespace AustriaSmartCardAPI
{
    class SmartCardSign
    {
        private static void Main()
        {
            string hashedString = null;

            using (var context = new SCardContext())
            {
                String readerName = getReaderList(context);
                if (readerName == null)
                {
                    return;
                }
                using (
                    IsoReader isoReader = new IsoReader(context, readerName, SCardShareMode.Shared, SCardProtocol.Any,
                        false))
                {
                    hashedString = signPOSString(isoReader, "123456", "Test string", 1);
                }
                context.Release();
            }
            Console.ReadKey();
        }

        public static string signPOSString(IsoReader isoReader, string pin, string textString, int type)
        {
            byte[] signature = null, text, hash;
            bool signatureCheck;
            string hashString;

            ICashRegisterSmartCard smartCard = CashRegisterSmartCardFactory.createInstance(isoReader);

            String certificateSerialDecimal = smartCard.readCertificateSerialDecimal();
            String certificateSerialHex = smartCard.readCertificateSerialHex();


            X509Certificate2 cert = smartCard.readCertificate();

            /* check for the loading of the certificate */
            if (cert == null)
            {
                return (null);
            }

            text = Encoding.Unicode.GetBytes(textString);
            SHA256Managed crypt = new SHA256Managed();
            hash = crypt.ComputeHash(text, 0, text.Length);
            crypt.Dispose();
            hashString = Convert.ToBase64String(hash);

            /* check what type of signature to use  - with selection or without selection */
            if (type == 1)
            {
                /* with selection */
                signature = smartCard.sign(pin, hash);

                /* verify the signature */
                signatureCheck = smartCard.verify(signature, text);
            }
            else
            {
                /* signature without selection */
                smartCard.prepareSignature();
                signature = smartCard.signWithoutSelection(pin, hash);

                /* verify the signature */
                signatureCheck = smartCard.verify(signature, text);
            }

            if (signatureCheck)
                return (hashString);
            else
                return (null);
        }

        private static String getReaderList(SCardContext context)
        {
            context.Establish(SCardScope.System);
            var readerNames = context.GetReaders();
            if (readerNames == null || readerNames.Length < 1)
            {
                return null;
            }
            return readerNames[0];
        }


    }
}


